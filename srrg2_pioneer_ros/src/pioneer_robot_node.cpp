#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include "pioneer_library/pioneer_robot.h"
#include "yaml-cpp/yaml.h"
#include <fstream>
#include "timestamp_filter.h"
#include <iomanip>

using namespace std;
using namespace srrg_pioneer;

struct StaticTransformInfo {
  std::string reference;
  std::string target;
  tf::Transform transform;
};

std::list<StaticTransformInfo> pub_transforms;

PioneerRobot robot;
volatile double tv = 0, rv = 0;
void commandVelCallback(const geometry_msgs::TwistConstPtr twist){
  tv = twist->linear.x;
  rv = twist->angular.z;
}

int main(int argc, char** argv) {
  std::string serial_device = "/dev/ttyUSB0";
  std::string odom_topic = "/odom";
  std::string odom_frame_id="/odom";
  std::string base_link_frame_id="/base_link";
  std::string cmd_vel_topic="/cmd_vel";
  std::string robot_type = "p3at";
  int time_filter_calib_rounds = 100;
  bool publish_tf = true;
  
  ros::init(argc, argv, "pioneer_robot_node");
  ros::NodeHandle nh("~");

  if (argc>1){
    std::ifstream fin(argv[1]);
    if (fin.fail()) {
      ROS_ERROR("Pioneer could not open %s.", argv[1]);
      exit(-1);
    }
#ifdef HAVE_YAMLCPP_GT_0_5_0
    // The document loading process changed in yaml-cpp 0.5.
    YAML::Node doc = YAML::Load(fin);
#else
    YAML::Parser parser(fin);
    YAML::Node doc;
    parser.GetNextDocument(doc);
#endif

    serial_device = doc["serial_device"].as<std::string>();
    odom_topic    = doc["odom_topic"].as<std::string>();
    odom_frame_id = doc["odom_frame_id"].as<std::string>();
    base_link_frame_id = doc["base_link_frame_id"].as<std::string>(); 
    robot_type = doc["robot_type"].as<std::string>(); 
    cmd_vel_topic = doc["cmd_vel_topic"].as<std::string>();
    publish_tf = doc["publish_tf"].as<bool>();
    time_filter_calib_rounds = doc["time_filter_calib_rounds"].as<int>();
    YAML::Node tf_node = doc["tf"];
    int k = 0;
    for (YAML::const_iterator tf_it= tf_node.begin(); tf_it!=tf_node.end(); tf_it++ ) {
      StaticTransformInfo info;
      std::cerr << "tf[" << k << "]" << std::endl;
      const YAML::Node& tf=*tf_it;
      info.reference=tf["reference"].as<std::string>();
      info.target=tf["target"].as<std::string>();
      const YAML::Node& translation=tf["translation"];
      tf::Vector3 t;
      t.setX(translation[0].as<float>());
      t.setY(translation[1].as<float>());
      t.setZ(translation[2].as<float>());
      const YAML::Node& rotation=tf["rotation"];
      tf::Quaternion q;
      q.setX(rotation[0].as<float>());
      q.setY(rotation[1].as<float>());
      q.setZ(rotation[2].as<float>());
      q.setW(rotation[3].as<float>());
      info.transform.setOrigin(t);
      info.transform.setRotation(q);
      pub_transforms.push_back(info);
      ++k;
    }
  }
  
  cerr << "running with params: ";
  cerr << "serial_device: " << serial_device << endl;
  cerr << "odom_topic: " << odom_topic << endl;
  cerr << "odom_frame_id: " << odom_frame_id << endl;
  cerr << "base_link_frame_id: " << base_link_frame_id << endl;
  cerr << "cmd_vel_topic: " << cmd_vel_topic << endl;
  cerr << "robot type: " << robot_type << endl; 
  cerr << "publish_tf: " << publish_tf << endl;
  cerr << "time_filter_calib_rounds: " << time_filter_calib_rounds << endl;

  TimestampFilter timestamp_filter;
  timestamp_filter.setCalibrationRounds(time_filter_calib_rounds);

  ros::Subscriber command_vel_subscriber = nh.subscribe<geometry_msgs::TwistConstPtr>(cmd_vel_topic, 1, &commandVelCallback);
  ros::Publisher odom_publisher = nh.advertise<nav_msgs::Odometry>(odom_topic, 1);
  robot.connect(robot_type, serial_device);
  nav_msgs::Odometry odom;
  odom.header.frame_id = odom_frame_id;

  tf::TransformBroadcaster br;

  int seq = 0;
  bool first_round = true;
  // ofstream os("stocazzo.txt");
  // os << std::fixed;
  // os.precision(9);

  while(ros::ok()){
    ros::spinOnce();
    robot.spinOnce();
    robot.setSpeed(tv,rv);
    // send the odometry
    double x,y,theta;
    robot.getOdometry(x,y,theta);

    ros::Time this_time=ros::Time::now();
    TimestampFilter::Status previous_timefilter_status=timestamp_filter.status();
    
    timestamp_filter.setMeasurement(this_time.toSec());
    
    if (first_round) {
      std::cerr << "Timefilter: initializing" << std::endl;
      first_round=false;
    }

    TimestampFilter::Status timefilter_status = timestamp_filter.status();
    if (timestamp_filter.status()!=TimestampFilter::Status::Ready) {
      std::cerr << ".";
      continue;
    }
    if (timefilter_status!=previous_timefilter_status) {
      std::cerr << std::endl << "TimeFilter initialized, delta: " << timestamp_filter.delta() << std::endl;
    }
    
    // os << this_time.toSec() << " ";
    this_time.fromSec(timestamp_filter.stamp());
    // os << this_time.toSec() << endl;

    odom.header.seq = seq;
    odom.header.stamp = this_time;
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0;
    double s = sin (theta/2.0);
    double c = cos (theta/2.0);
    odom.pose.pose.orientation.x = 0;
    odom.pose.pose.orientation.y = 0;
    odom.pose.pose.orientation.z = s;
    odom.pose.pose.orientation.w = c;
    odom_publisher.publish(odom);

    if (publish_tf) {
      tf::Transform transform;
      transform.setOrigin( tf::Vector3(odom.pose.pose.position.x, odom.pose.pose.position.y, 0.0) );
      tf::Quaternion q;
      q.setRPY(0, 0, theta);
      transform.setRotation(q);
      br.sendTransform(tf::StampedTransform(transform, this_time, odom_frame_id, base_link_frame_id));
      for (const auto& it: pub_transforms) {
        br.sendTransform(tf::StampedTransform(it.transform,
                                              this_time,
                                              it.reference,
                                              it.target));
      }
    }
    
    ++seq;
  }
  cerr << "Shutting down" << endl;
  robot.disconnect();
}
