#include "pioneer_robot.h"
#include "pioneer_lib.h"
#include <cmath>
#include <iostream>

namespace srrg_pioneer {

  using namespace std;

  PioneerRobot::PioneerRobot(){
    _x=_y=_theta=_tv=_rv=_des_tv=_des_rv=0;
    _update_timestamp=0;
  }

  PioneerRobot::~PioneerRobot(){
    disconnect();
    _x=_y=_theta=_tv=_rv=_des_tv=_des_rv=0;
  }

  void PioneerRobot::connect(const std::string& model, const std::string& device) {
    pioneer_base_direct_initialize_robot(model.c_str(), device.c_str());
    for (int i =0; i<1; ++i)
      pioneer_base_direct_query_config();
    pioneer_base_direct_sonar_off();
    pioneer_base_direct_set_acceleration(1);
    pioneer_base_direct_set_deceleration(5);

  }

  void PioneerRobot::disconnect() {
    pioneer_base_direct_shutdown_robot();
  }

  void PioneerRobot::setSpeed(double tv, double rv) {
    _des_tv=tv;
    _des_rv=rv;
    pioneer_base_direct_set_velocity(_des_tv, _des_rv);
  }

  void PioneerRobot::getOdometry(double&x, double& y, double& theta){
    x=_x;
    y=_y;
    theta=_theta;
  }

  void PioneerRobot::getSpeed(double&tv, double& rv){
    tv=_tv;
    rv=_rv;
  }

  static const float cos_coeffs[]={0., 0.5 ,  0.    ,   -1.0/24.0,   0.    , 1.0/720.0};
  static const float sin_coeffs[]={1., 0.  , -1./6. ,      0.    ,   1./120, 0.   };
  static void _computeThetaTerms(float* sin_theta_over_theta,
                               float* one_minus_cos_theta_over_theta,
                               float theta) {
  // evaluates the taylor expansion of sin(x)/x and (1-cos(x))/x,
  // where the linearization point is x=0, and the functions are evaluated
  // in x=theta
  *sin_theta_over_theta=0;
  *one_minus_cos_theta_over_theta=0;
  float theta_acc=1;
  for (uint8_t i=0; i<6; i++) {
    if (i&0x1)
      *one_minus_cos_theta_over_theta+=theta_acc*cos_coeffs[i];
    else 
      *sin_theta_over_theta+=theta_acc*sin_coeffs[i];
    theta_acc*=theta;
  }
}

  void PioneerRobot::spinOnce(){
    pioneer_base_direct_update_status(&_update_timestamp);
   
    double displacement, dtheta=0;
    pioneer_base_direct_get_state(&displacement, &dtheta, &_tv, &_rv);
    float one_minus_cos_theta_over_theta, sin_theta_over_theta;
    _computeThetaTerms(&sin_theta_over_theta, &one_minus_cos_theta_over_theta, dtheta);
    
    float dx=displacement*sin_theta_over_theta;
    float dy=displacement*one_minus_cos_theta_over_theta;

    double s = sin(_theta);
    double c = cos(_theta);
    _x+=c*dx-s*dy;
    _y+=s*dx+c*dy;
    _theta+=dtheta;
    if (_theta>M_PI)
      _theta-=2*M_PI;
    else if (_theta<-M_PI)
      _theta+=2*M_PI;
  }

}
